﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class alkupiste : MonoBehaviour {

	public GameObject hahmo1;
	public GameObject hahmo2;
	public GameObject hahmo3;
	public GameObject hahmo4;
	public GameObject hahmo5;
	public UnityEngine.Avatar avatar1;
	public UnityEngine.Avatar avatar2;
	public UnityEngine.Avatar avatar3;
	public UnityEngine.Avatar avatar4;
	public UnityEngine.Avatar avatar5;
	public GameObject kontrolleri;

	// Use this for initialization
	void Start () {
		hahmo1.SetActive (false);
		hahmo2.SetActive (false);
		hahmo3.SetActive (false);
		hahmo4.SetActive (false);
		hahmo5.SetActive (false);
		Debug.Log ("hahmo = " + pelaajadata.hahmo.ToString ());
		Animator animator = kontrolleri.GetComponent<Animator> ();
		if (pelaajadata.hahmo == 0) {
			hahmo1.SetActive (true);
			animator.avatar = avatar1;
			kontrolleri.GetComponent<ThirdPersonCharacter>().SetMoveMultiplier(2.0f);
		}
		if (pelaajadata.hahmo == 1) {
			hahmo2.SetActive (true);
			animator.avatar = avatar2;
		}
		if (pelaajadata.hahmo == 2) {
			hahmo3.SetActive (true);
			animator.avatar = avatar3;
		}
		if (pelaajadata.hahmo == 3) {
			hahmo4.SetActive (true);
			animator.avatar = avatar4;
		}
		if (pelaajadata.hahmo == 4) {
			hahmo5.SetActive (true);
			animator.avatar = avatar5;
		}
		if (PlayerPrefs.HasKey("x")) {
			GameObject The = GameObject.Find("ThirdPersonController");
			float x = PlayerPrefs.GetFloat ("x");
			float y = PlayerPrefs.GetFloat ("y");
			float z = PlayerPrefs.GetFloat ("z");
			Debug.Log (x.ToString ());
			Debug.Log (y.ToString ());
			Debug.Log (z.ToString ());

			The.transform.position = new Vector3 (x, y + 1.0f, z);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
