﻿using UnityEngine;
using System.Collections;

public class doorsound : MonoBehaviour {

    private GameObject DoorToOtherScenes = null;
    public AudioSource[] aanet;
    public AudioSource aani1;
    public AudioSource aani2;

	// Use this for initialization
	void Start () 
    {
        this.DoorToOtherScenes = GameObject.Find("DoorToOtherScenes");
        aanet = this.GetComponents<AudioSource>();
        aani1 = aanet[0];
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnTriggerEnter(Collider other)
    {
        aani1.Play();
    }
    void OnTriggerExit(Collider other)
    {
        aani1.Play();
    }

}
