﻿using UnityEngine;
using System.Collections;

public class collision : MonoBehaviour {

	public GameObject twine;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {

			twine.SetActive(true);

		}
	}

	void OnTriggerExit(Collider other) {
		if (other.gameObject.tag == "Player") {

			twine.SetActive(false);
			
		}
	}

}
