﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Etsi_tallenne : MonoBehaviour {

	int hahmo;

	void Start () {
		GameObject nappi = GameObject.Find ("jatka_pelia");

		// Etsitään onko jo olemassa pelihahmo
		if (PlayerPrefs.HasKey ("hahmo")) {
			// jos on, pelaaja voi aloittaa uuden tai jatkaa aiempaa peliä
			this.GetComponent <Text> ().text = "Jatka peliä tai aloita uusi.";
		} else {
			// jos ei ole, pelaajan pitää aloittaa uusi, piilotetaan Jatka peliä -nappi
			this.GetComponent <Text> ().text = "Aiempaa peliä ei löydy.";
			nappi.SetActive (false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
