﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class radanalkuunpalautus : MonoBehaviour {

	private GameObject[] sillat;
	private GameObject Ethan;
	private GameObject Maali;
	private bool[] siltatrigger;
	private int[] siltatype;
	private const int siltamaara = 9;
	List<string> kysymykset;
	public GameObject kys1, kys2, kys3;
	public GameObject vast1a, vast1b, vast1c;
	public GameObject vast2a, vast2b, vast2c;
	public GameObject vast3a, vast3b, vast3c;

	// Use this for initialization
	void Start () {
		siltatrigger = new bool[siltamaara];   // c# alustaa muuttujat oletusarvoisesti false tai nolla
		siltatype = new int[siltamaara] {2, 2, 2, 1, 1, 1, 3, 3, 3};
		sillat = new GameObject[siltamaara];
		sillat[0] = GameObject.Find ("Silta1a");
		sillat[1] = GameObject.Find ("Silta1b");
		sillat[2] = GameObject.Find ("Silta1c");
		sillat[3] = GameObject.Find ("Silta2a");
		sillat[4] = GameObject.Find ("Silta2b");
		sillat[5] = GameObject.Find ("Silta2c");
		sillat[6] = GameObject.Find ("Silta3a");
		sillat[7] = GameObject.Find ("Silta3b");
		sillat[8] = GameObject.Find ("Silta3c");
		Ethan = GameObject.Find ("ThirdPersonController");
		Maali = GameObject.Find ("Maali");

		// Kysymykset muodossa "kysymys & vastaus1 & vastaus2 & vastaus3 & oikea vastaus", mikä string pilkotaan & erottimella, oikea vastaus 1, 2 tai 3
		kysymykset = new List<string>();
		kysymykset.Add ("Kuinka paljon työnantaja pidättää palkastasi veroa, jos et toimita verokorttia työnantajalle? "+
			"& 33.00% & 25.00% & 60% & 3");
		kysymykset.Add ("Mistä valtion laitoksesta voit hakea työttömyyspäivärahaa, jos jäät työttömäksi etkä kuulu työttömyyskassaan? "+
			"& Kelasta & Oikeusasiamieheltä & Valtionkonttorista & 1");
		kysymykset.Add ("Mikä on suomalaisten keskipalkka kuukaudessa? "+
			"& Noin 2 960 euroa & Noin 3 960 euroa & Noin 1 960 euroa & 1");
		kysymykset.Add ("Mikä seuraavista ei saa vaikuttaa palkkaan? "+
			"& Kokemus & Sukupuoli & Ammattitaito & 2");
		kysymykset.Add ("Mikä vaikuttaa siihen, pitääkö kesätöistä maksaa veroa? "+
			"& Palkan suuruus & Yrityksen koko & Vanhempien kokonaisansio & 1");
		kysymykset.Add ("Kuinka paljon sunnuntaityöstä on maksettava? "+
			"& Puolitoistakertainen palkka & Kaksinkertainen palkka & Normaali palkka, jos sunnuntain työaika ei ylitä kahdeksaa tuntia & 2");
		kysymykset.Add ("Mihin osiin vanhuuseläke jakautuu? "+
			"& Valtion- ja kunnaneläkkeeseen & Työ-, kansan- ja takuueläkkeeseen & Sopimus- ja vapaaehtoiseläkkeeseen & 2");
		kysymykset.Add ("Maksetaanko kesäloman ajalta palkkaa? "+
			"& Vain jos työsopimuksessa on niin sovittu & Kyllä, kaikille & Vain johtavassa asemassa oleville & 2");

		// 1. satunnainen kysymys teksteihin ja silta kuntoon
		// rand ja decr luo satunnaishashin ja sen palautuksen, millä arvotaan vastausten paikat
		int[] rand = new int[3];
		int[] decr = new int[3];
		rand[0] = Random.Range(0, 3);       // rand[0] on satunnaisluku 0, 1 tai 2
		rand[1] = rand[0];
		while (rand[0] == rand[1])          // rand[1] on satunnaisluku 0, 1 tai 2 kunhan se ei ole sama kuin rand[0]
			rand[1] = Random.Range(0, 3);
		rand[2] = rand[0];                  // rand[2] on satunnaisluku 0, 1 tai 2 kunhan se ei ole sama kuin rand[0] tai rand[1]
		while ((rand[0] == rand[2]) || (rand[1] == rand[2]))
			rand[2] = Random.Range(0, 3);		
		decr [rand [0]] = 0;                // ja decr on rand-taulukon käänteistaulukko
		decr [rand [1]] = 1;
		decr [rand [2]] = 2;
		int k1 = Random.Range(0, kysymykset.Count);
		string[] k1text = kysymykset[k1].Split ('&');
		kys1.GetComponent <TextMesh> ().text = k1text [0];
		vast1a.GetComponent <TextMesh> ().text = k1text [rand[0]+1];
		vast1b.GetComponent <TextMesh> ().text = k1text [rand[1]+1];
		vast1c.GetComponent <TextMesh> ().text = k1text [rand[2]+1];
		siltatype[0 * 3 + decr[int.Parse(k1text[4])- 1]] = 0;

		// 2. satunnainen kysymys teksteihin ja silta kuntoon
		rand[0] = Random.Range(0, 3);
		rand[1] = rand[0];
		while (rand[0] == rand[1])
			rand[1] = Random.Range(0, 3);
		rand[2] = rand[0];
		while ((rand[0] == rand[2]) || (rand[1] == rand[2]))
			rand[2] = Random.Range(0, 3);		
		decr [rand [0]] = 0;
		decr [rand [1]] = 1;
		decr [rand [2]] = 2;
		int k2 = k1;
		while (k1 == k2)
			k2 = Random.Range(0, kysymykset.Count);
		string[] k2text = kysymykset[k2].Split ('&');
		kys2.GetComponent <TextMesh> ().text = k2text [0];
		vast2a.GetComponent <TextMesh> ().text = k2text [rand[0]+1];
		vast2b.GetComponent <TextMesh> ().text = k2text [rand[1]+1];
		vast2c.GetComponent <TextMesh> ().text = k2text [rand[2]+1];
		siltatype[1 * 3 + decr[int.Parse(k2text[4])- 1]] = 0;

		// 3. satunnainen kysymys teksteihin ja silta kuntoon
		rand[0] = Random.Range(0, 3);
		rand[1] = rand[0];
		while (rand[0] == rand[1])
			rand[1] = Random.Range(0, 3);
		rand[2] = rand[0];
		while ((rand[0] == rand[2]) || (rand[1] == rand[2]))
			rand[2] = Random.Range(0, 3);		
		decr [rand [0]] = 0;
		decr [rand [1]] = 1;
		decr [rand [2]] = 2;
		int k3 = k1;
		while ((k1 == k3) || (k2 == k3))
			k3 = Random.Range(0, kysymykset.Count);
		string[] k3text = kysymykset[k3].Split ('&');
		kys3.GetComponent <TextMesh> ().text = k3text [0];
		vast3a.GetComponent <TextMesh> ().text = k3text [rand[0]+1];
		vast3b.GetComponent <TextMesh> ().text = k3text [rand[1]+1];
		vast3c.GetComponent <TextMesh> ().text = k3text [rand[2]+1];
		siltatype[2 * 3 + decr[int.Parse(k3text[4])- 1]] = 0;

	}
	
	// Update is called once per frame
	void Update () {
		// jos tippuu alas, niin nostetaan takaisin alkupisteeseen
			if (Ethan.transform.position.y < 10) {
			float x = Ethan.transform.position.x;
			float y = Ethan.transform.position.y;
			float z = Ethan.transform.position.z;
			Ethan.transform.position = new Vector3 ((float)1900, (float)20, (float)2643); 
		}

		// jos kulkee sillalla, niin triggeröidään oikea silta
		// 1. silta
		if ((Ethan.transform.position.x > 1923) && (Ethan.transform.position.x < 1926)) {
			if (Ethan.transform.position.z > 2649)
				siltatrigger [0] = true;
			else
				if (Ethan.transform.position.z < 2641)
					siltatrigger [2] = true;
				else
					siltatrigger [1] = true;
		}
		// 2. silta
		if ((Ethan.transform.position.x > 1965) && (Ethan.transform.position.x < 1968)) {
			if (Ethan.transform.position.z > 2649)
				siltatrigger [3] = true;
			else
				if (Ethan.transform.position.z < 2641)
					siltatrigger [5] = true;
				else
					siltatrigger [4] = true;
		}
		// 3. silta
		if ((Ethan.transform.position.x > 2007) && (Ethan.transform.position.x < 2010)) {
			if (Ethan.transform.position.z > 2649)
				siltatrigger [6] = true;
			else
				if (Ethan.transform.position.z < 2641)
					siltatrigger [8] = true;
				else
					siltatrigger [7] = true;
		}

		// käydään läpi kaikki sillat ja liikutetaan niitä jotka on triggeröity
		for (int i = 0; i < siltamaara; i++) {
			if (siltatrigger [i]) {
				// siltatyyppi 1 poistaa sillan
				if (siltatype [i] == 1) {
					sillat [i].active = false;
				}
				if (siltatype [i] == 2) {
					sillat [i].transform.RotateAround (new Vector3(sillat[i].transform.position.x, sillat[i].transform.position.y, sillat[i].transform.position.z), 
						Vector3.left, Time.deltaTime*150);
				}
				if (siltatype [i] == 3) {
					sillat [i].transform.RotateAround (new Vector3(sillat[i].transform.position.x, sillat[i].transform.position.y, sillat[i].transform.position.z), 
						Vector3.back, -Time.deltaTime*75);
				}
			}
		}

		Maali.transform.RotateAround (new Vector3(Maali.transform.position.x, Maali.transform.position.y, Maali.transform.position.z), 
			Vector3.up, Time.deltaTime*150);

	}
}

