﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class pallopeli : MonoBehaviour {

	GameObject p1;
	GameObject p2;
	GameObject p3;
	GameObject p4;
	GameObject teksti;
	bool pallo1 = false;
	bool pallo2 = false;
	bool pallo3 = false;
	bool pallo4 = false;
	bool notdone = true;
	GameObject canvas;
	GameObject infoteksti;
	GameObject jp1;
	GameObject jp2;
	GameObject jp3;
	GameObject jp4;
	GameObject Ethan;


	// Use this for initialization
	void Start () {
		p1 = GameObject.Find ("jalkapallo1");
		p2 = GameObject.Find ("jalkapallo2");
		p3 = GameObject.Find ("jalkapallo3");
		p4 = GameObject.Find ("jalkapallo4");
		teksti = GameObject.Find ("Infoteksti");
		canvas = GameObject.Find("Canvas");
		canvas.SetActive (false);
		infoteksti = GameObject.Find ("Infoteksti");
		jp1 = GameObject.Find ("jalkapallo1");
		jp2 = GameObject.Find ("jalkapallo2");
		jp3 = GameObject.Find ("jalkapallo3");
		jp4 = GameObject.Find ("jalkapallo4");
		Ethan = GameObject.Find ("ThirdPersonController");
	}
	
	// Update is called once per frame
	void Update () {
		if (jp1.transform.position.y < -10) pallo1 = true;
		if (jp2.transform.position.y < -10) pallo2 = true;
		if (jp3.transform.position.y < -10) pallo3 = true;
		if (jp4.transform.position.y < -10) pallo4 = true;
		if (pallo1 && pallo2 && pallo3 && pallo4) {
			canvas.SetActive (true);
			infoteksti.GetComponent<TextMesh>().text = "Tehtävä suoritettu!";
		}
		if (Ethan.transform.position.y < 10) {
			float x = Ethan.transform.position.x;
			float y = Ethan.transform.position.y;
			float z = Ethan.transform.position.z;
			Ethan.transform.position = new Vector3 ((float)1900, (float)20, (float)2643); 
		}
	}
}
