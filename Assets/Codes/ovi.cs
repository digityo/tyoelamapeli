﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ovi : MonoBehaviour {
	public int hyppykohde;
	public bool muistapaikka;

	// Use this for initialization
	void OnTriggerEnter (Collider other) {
		GameObject.Find("Infoteksti").GetComponent<TextMesh>().text = "Mene sisälle painamalla enter!";
	}
	void OnTriggerExit (Collider other) {
		GameObject.Find("Infoteksti").GetComponent<TextMesh>().text = "";
	}
	void OnTriggerStay (Collider other) {
		if (Input.GetKeyDown("return")) {
			if (muistapaikka) {
				GameObject The = GameObject.Find ("ThirdPersonController");
				PlayerPrefs.SetFloat ("x", The.transform.position.x);
				PlayerPrefs.SetFloat ("y", The.transform.position.y);
				PlayerPrefs.SetFloat ("z", The.transform.position.z);
				Debug.Log (The.transform.position.x.ToString ());
				Debug.Log (The.transform.position.y.ToString ());
				Debug.Log (The.transform.position.z.ToString ());
			}
			SceneManager.LoadScene(hyppykohde);
		}

	}

	// Update is called once per frame
	void Update () {
//		if (Input.GetKeyDown("return")) {
//			SceneManager.LoadScene(4);
//		}
	}

}
