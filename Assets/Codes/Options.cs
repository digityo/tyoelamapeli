﻿using UnityEngine;
using System.Collections;

public class Options : MonoBehaviour 
{

    private GameObject ThirdPersonCam = null;
    //Kameran piirtoetäisyyden arvo
    private float cameradistance = 150f;
    
    //Piirtoetäisyyden näyttämiseen tehdyt muuttujat. Distancetext tehty että muuttujat saadaan interfacessa 10-100% arvoina.
    private float distancetext = 20;
    private GameObject displaydistance = null;

	private GameObject peliohjeet;

	// Use this for initialization
	void Start () 
    {
		peliohjeet = GameObject.Find("Peliohjeet");
		this.ThirdPersonCam = GameObject.Find("3rdPersonCam");
        this.displaydistance = GameObject.Find("Textdistance");
		peliohjeet.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () 
    {
        //Näppäimestä O piirtoetäisyys laskee -50f
        if (Input.GetKeyDown(KeyCode.O))
        {
            if (this.cameradistance > 100f)
            {
                this.cameradistance = this.cameradistance - 50f;
                this.ThirdPersonCam.GetComponent<Camera>().farClipPlane = this.cameradistance;
                
                this.distancetext = this.distancetext - 10;
                this.displaydistance.GetComponent<TextMesh>().text = "Piirtoetäisyys: " + this.distancetext + "%";
            }

            if (this.cameradistance == 100f)
            {
                this.cameradistance = 100f;
                this.displaydistance.GetComponent<TextMesh>().text = "Piirtoetäisyys: " + this.distancetext + "%";
            }
        }

        //Näppäimestä P piirtoetäisyys nousee +50f
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (this.cameradistance < 550f)
            {
                this.cameradistance = this.cameradistance + 50f;
                this.ThirdPersonCam.GetComponent<Camera>().farClipPlane = this.cameradistance;

                this.distancetext = this.distancetext + 10;
                this.displaydistance.GetComponent<TextMesh>().text = "Piirtoetäisyys: " + this.distancetext + "%";
            }
            if(this.cameradistance > 550f)
            {
                this.cameradistance = 550f;
                this.displaydistance.GetComponent<TextMesh>().text = "Piirtoetäisyys maksimissa";
            }

        }


		if (Input.GetKeyDown (KeyCode.H)) {
			peliohjeet.SetActive (!peliohjeet.activeSelf);
		}
	}
}
