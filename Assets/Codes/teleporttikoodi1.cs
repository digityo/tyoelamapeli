﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class teleporttikoodi1 : MonoBehaviour
{
    private GameObject ThirdPersonController = null;

    public AudioSource[] sounds;
    public AudioSource noise1;
    public AudioSource noise2;

    // Use this for initialization
    void Start()
    {
        this.ThirdPersonController = GameObject.Find("ThirdPersonController");
        sounds = this.GetComponents<AudioSource>();
        noise1 = sounds[0];

    }

    // Update is called once per frame
    void Update()
    {
        //Puisto
        if (Input.GetKeyDown(KeyCode.F1))
        {
            this.ThirdPersonController.transform.position = new Vector3(2108f, 20f, 2785.39f);
            noise1.Play();
        }
        //Tori
        if (Input.GetKeyDown(KeyCode.F2))
        {
            this.ThirdPersonController.transform.position = new Vector3(2005f, 20f, 2506f);
            noise1.Play();
        }
        //Kirkko
        if (Input.GetKeyDown(KeyCode.F3))
        {
            this.ThirdPersonController.transform.position = new Vector3(1707.2f, 30f, 2655.3f);
            noise1.Play();
        }
        //Urheilukenttä
        if (Input.GetKeyDown(KeyCode.F4))
        {
            this.ThirdPersonController.transform.position = new Vector3(1652.9f, 18f, 2432.7f);
            noise1.Play();
        }

        //Paluu aloitusvalikkoon
        if (Input.GetKey("escape"))
        {
            SceneManager.LoadScene(0);
        }

    }
}