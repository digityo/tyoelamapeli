using UnityEngine;
using System.Collections;

public class tietokonetrigger: MonoBehaviour {

	public GameObject tietokoneruutu;

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {

			tietokoneruutu.SetActive(true);

		}
	}

	void OnTriggerExit(Collider other) {
		if (other.gameObject.tag == "Player") {

			tietokoneruutu.SetActive(false);

		}
	}

}
