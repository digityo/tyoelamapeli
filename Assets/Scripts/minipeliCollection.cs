﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class minipeliCollection : MonoBehaviour {

	public List<GameObject> portit;

	// Use this for initialization
	void Start () {
		Debug.Log ("Minipelikontrolleri");
		// kaikki portit pois päältä paitsi ne jotka on määritelty olemaan heti alussa + pelaajadatasta löytyvät
		foreach (GameObject portti in portit) {
			portti.SetActive (false);
		}
		foreach (pelidata.minipeli rata in pelidata.pelit) {
			Debug.Log (rata.nimi);
			if (rata.start) {
				portit [rata.index].SetActive (true);
			}
		}
			
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
