﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class hahmonvaihto : MonoBehaviour {

	public GameObject hahmo1;
	public GameObject hahmo2;
	public GameObject hahmo3;
	public UnityEngine.Avatar avatar1;
	public UnityEngine.Avatar avatar2;
	public UnityEngine.Avatar avatar3;
	public GameObject kontrolleri;

	// Use this for initialization
	void Start () {
		hahmo1.SetActive (false);
		hahmo2.SetActive (false);
		hahmo3.SetActive (false);
		Debug.Log ("hahmo = " + pelaajadata.hahmo.ToString ());
		Animator animator = kontrolleri.GetComponent<Animator> ();
		if (pelaajadata.hahmo == 0) {
			hahmo1.SetActive (true);
			animator.avatar = avatar1;
			kontrolleri.GetComponent<ThirdPersonCharacter>().SetMoveMultiplier(2.0f);
		}
		if (pelaajadata.hahmo == 1) {
			hahmo2.SetActive (true);
			animator.avatar = avatar2;
		}
		if (pelaajadata.hahmo == 2) {
			hahmo3.SetActive (true);
			animator.avatar = avatar3;
		}

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
