using System.Collections.Generic;

public static class pelidata {

	public class minipeli {
		public string nimi;
		public int index;
		public bool uusi;
		public bool suoritettu;
		public bool start;
		public bool suoritettavana;
		public string kysymykset;

		public minipeli(string uusi_nimi, int uusi_index, bool uusi_start, bool uusi_suoritettavana, string uudet_kysymykset) {
			nimi = uusi_nimi;
			index = uusi_index;
			uusi = true;
			suoritettu = false;
			start = uusi_start;
			suoritettavana = uusi_suoritettavana;
			kysymykset = uudet_kysymykset;
		}
	}

	public static List<minipeli> pelit = new List<minipeli>();

}
