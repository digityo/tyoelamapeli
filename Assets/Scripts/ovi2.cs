using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ovi2 : MonoBehaviour {
	public string hyppykohde = "";

	// Use this for initialization
	void OnTriggerEnter (Collider other) {
		GameObject.Find("MeneSisaanTeksti").GetComponent<TextMesh>().text = "Mene sisälle painamalla enter!";
		hyppykohde = "kirkko";
	}
	void OnTriggerExit (Collider other) {
		GameObject.Find("MeneSisaanTeksti").GetComponent<TextMesh>().text = "";
		hyppykohde = "kirkko";
	}
	void OnTriggerStay (Collider other) {
		if (Input.GetKeyDown("return")) {
			SceneManager.LoadScene(3);
		}

	}
	// Update is called once per frame
	void Update () {
	}

}
