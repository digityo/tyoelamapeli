using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityStandardAssets.Characters.ThirdPerson;

public class te_tietokone : MonoBehaviour {

	public GameObject hahmo1;
	public GameObject hahmo2;
	public GameObject hahmo3;
	public UnityEngine.Avatar avatar1;
	public UnityEngine.Avatar avatar2;
	public UnityEngine.Avatar avatar3;
	public GameObject kontrolleri;

	public GameObject teksti;
	public GameObject tietokoneruutu;

	int max;
	int kysymys = 0;
	string tekstit = "Hei! Minä olen tietokone.\nEtsitään sinulle työpaikka, vastaa seuraaviin:\n\n";
	string[] kysymyslista;
	bool ready;
	int vaihtoehto, vaihtoehto1, vaihtoehto2, vaihtoehtoja;

	// Use this for initialization
	void Start () {
		hahmo1.SetActive (false);
		hahmo2.SetActive (false);
		hahmo3.SetActive (false);
		Debug.Log ("hahmo = " + pelaajadata.hahmo.ToString ());
		Animator animator = kontrolleri.GetComponent<Animator> ();
		if (pelaajadata.hahmo == 0) {
			hahmo1.SetActive (true);
			animator.avatar = avatar1;
			kontrolleri.GetComponent<ThirdPersonCharacter>().SetMoveMultiplier(2.0f);
		}
		if (pelaajadata.hahmo == 1) {
			hahmo2.SetActive (true);
			animator.avatar = avatar2;
		}
		if (pelaajadata.hahmo == 2) {
			hahmo3.SetActive (true);
			animator.avatar = avatar3;
		}

		tietokoneruutu.SetActive (false);

		// testausta varten täällä, tehdään varsinaisesti alustus.cs
//		pelidata.pelit.Add (new pelidata.minipeli ("TE-palvelut", 0, true, false, ""));
//		pelidata.pelit.Add (new pelidata.minipeli ("Urheiluvälinesiivooja", 7, false, true, "Oletko harrastanut urheilua?;Oletko omatoiminen?"));
//		pelidata.pelit.Add (new pelidata.minipeli ("Pysäköintiapulainen", 1, false, true, "Onko sinulla ajokorttia?"));

		string kysymykset = "";
		ready = false;
		int i = 0;
		int j = -1;
		int k = -1;
		while ((k < 0) && (i < pelidata.pelit.Count)) {
			teksti.GetComponent <Text> ().text = "yes";
			if (pelidata.pelit[i].suoritettavana) {
				if (j < 0)
					j = i;
				else {
					k = i;
					break;
				}
			}
			i++;
		}
	
		if (j < 0) {
//			teksti.GetComponent <Text> ().text = "Ei työpaikkoja tarjolla.";
		} else {
			kysymykset = kysymykset + pelidata.pelit [j].kysymykset;
			vaihtoehto1 = j;
			vaihtoehtoja = 1;
			if (k >= 0) {
				kysymykset = kysymykset + ";" + pelidata.pelit [k].kysymykset;
				vaihtoehto2 = k;
				vaihtoehtoja = 2;
			}
			teksti.GetComponent <Text> ().text = kysymykset;
		}
//		teksti.GetComponent <Text> ().text = k.ToString() + ", " + pelidata.pelit.Count.ToString();

		max = 1;
		kysymyslista = kysymykset.Split (';');
		tekstit = tekstit + kysymyslista[0] + "\nValitse: [1] kyllä, [2] en";
		teksti.GetComponent <Text> ().text = tekstit;
	}
	
	// Update is called once per frame
	void Update () {
		if (kysymys < kysymyslista.Length) { 
			if (Input.GetKeyDown ("1")) {
				tekstit = tekstit + "\nKyllä\n\n";
				kysymys++;
				tekstit = tekstit + kysymyslista [kysymys] + "\nValitse: [1] kyllä, [2] en";
				teksti.GetComponent <Text> ().text = tekstit;
				vaihtoehto = vaihtoehto1;
			}
			if (Input.GetKeyDown ("2")) {
				tekstit = tekstit + "\nEn\n\n";
				kysymys++;
				tekstit = tekstit + kysymyslista [kysymys] + "\nValitse: [1] kyllä, [2] en";
				teksti.GetComponent <Text> ().text = tekstit;
				if (vaihtoehtoja == 2)
					vaihtoehto = vaihtoehto2;
				else
					vaihtoehto = vaihtoehto1;
			}
		} else {
			if (!ready) {
				tekstit = tekstit + "\nMeillä on sinulle sopiva työ:\n" + pelidata.pelit [vaihtoehto].nimi;
				tekstit = tekstit + "\n\nEtsi se kartalta ja käy kysymässä!";
				teksti.GetComponent <Text> ().text = tekstit;
				pelidata.pelit [vaihtoehto].start = true;
				ready = true;

			}

		}
		Debug.Log ( tekstit.Split (Environment.NewLine.ToCharArray()).Length.ToString ());
		if (tekstit.Split (Environment.NewLine.ToCharArray()).Length > 10) {
			tekstit = tekstit.Substring (tekstit.IndexOf ("\n") + 1, tekstit.Length - tekstit.IndexOf ("\n")-1);
			teksti.GetComponent <Text> ().text = tekstit;
		}


	}
}
