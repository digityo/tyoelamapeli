﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class alkuvalikko : MonoBehaviour {

	float x = 0;
	float z = -10;
	public GameObject kamera;

	public void hahmonvalintaan () 
	{
		SceneManager.LoadScene(30);
	}

	public void jatkapelia () 
	{
		// ladataa hahmo pelaajadataan ja siirrytään pelimaailmaan
		pelaajadata.hahmo = PlayerPrefs.GetInt ("hahmo");
		pelaajadata.rahat = PlayerPrefs.GetInt ("rahat");
		SceneManager.LoadScene(1);
	}

	void Update () {
		kamera.transform.position = new Vector3 ((float) -Math.Sin(Time.time/7)/3, 1.0f- (float) Math.Sin(Time.time/7)/11,(float) (-10 - Math.Sin(Time.time/5)));
	}

}
