﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// valitaan pelihahmo ja alustetaan peli (pelin alustuksen voisi siirtää nätimpään paikkaan)
public class valitse_hahmo : MonoBehaviour {

	int hahmo = 2;
	int maxhahmo = 2;
	public GameObject valintaruutu;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("left")) {
			if (hahmo > 0) hahmo--;
		}
		if (Input.GetKeyDown("right")) {
			if (hahmo < maxhahmo) hahmo++;

		}
		valintaruutu.transform.position = new Vector3 ((float) hahmo-1f, 1.5f, 0.65f);
		//		Lerpillä laatikko liikkuisi sulavammin, mutta tähän testin perusteella ehkä sopii nopea hyppely 
		//		Huom. lerppiä käytetään näissä koodeissa paikoitellen vähän väärin. Viimeisen parametrin pitäisi olla kumulatiivinen aikamuuttuja, ei vakio.
		//      valintaruutu.transform.position = Vector3.Lerp( valintaruutu.transform.position, new Vector3 ((float) hahmo-1f, 1.5f, 0.65f), Time.deltaTime);

		if (Input.GetKeyDown("return") || Input.GetKeyDown("enter")) {
			// pelaajahahmon alustus ja peliin
			pelaajadata.hahmo = hahmo;
			pelaajadata.rahat = 0;
			if (PlayerPrefs.HasKey ("x")) {
				PlayerPrefs.DeleteKey ("x");
			}
			SceneManager.LoadScene(1);
		}


	}  
}
