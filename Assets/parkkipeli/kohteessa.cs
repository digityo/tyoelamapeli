﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class kohteessa : MonoBehaviour {
	public string hyppykohde = "";

	bool FullyContains(Collider resident)
	{
		Collider zone = GetComponent<Collider>();
		if(zone == null)
		{
			return false;
		}
		return zone.bounds.Contains(resident.bounds.max) && zone.bounds.Contains(resident.bounds.min);
	}

	// Use this for initialization
	void OnTriggerEnter (Collider other) {
		if (other.gameObject.name == "kori") {
			GameObject.Find ("infoteksti").GetComponent<TextMesh> ().text = "Melkein ruudussa!";
		}
		hyppykohde = "kirkko";
	}
	void OnTriggerExit (Collider other) {
		GameObject.Find("infoteksti").GetComponent<TextMesh>().text = "Aja keltaiseen ruutuun ja pysähdy!";
		hyppykohde = "kirkko";
	}
	void OnTriggerStay (Collider other) {
		if (other.gameObject.name == "kori") {
			if (FullyContains (other)) {
				if (GameObject.Find ("OmaAuto").GetComponent<Rigidbody> ().velocity.magnitude > 0.2) {
					GameObject.Find ("infoteksti").GetComponent<TextMesh> ().text = "Olet ruudussa. Pysähdy!";
				} else {
					GameObject.Find ("infoteksti").GetComponent<TextMesh> ().text = "Onnistuit! Paina Enter.";
					if (Input.GetKeyDown ("return")) {
						SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
					}
				} 
			} else {
				GameObject.Find ("infoteksti").GetComponent<TextMesh> ().text = "Melkein ruudussa!";
			}

		}
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey("escape"))
		{
			SceneManager.LoadScene(10);
		}

//		float z = Camera.main.gameObject.transform.position.z;
		if (Input.GetKey("1")) {
			float y = GameObject.Find ("Camera").GetComponent<Transform> ().position.y;
			if (y > 5) {
				GameObject.Find ("Camera").GetComponent<Transform> ().Translate (new Vector3 (0, 0, 1));
			}
		}
		if (Input.GetKey("2")) {
			float y = GameObject.Find ("Camera").GetComponent<Transform> ().position.y;
			if (y < 100) {
				GameObject.Find ("Camera").GetComponent<Transform> ().Translate (new Vector3 (0, 0, -1));
			}
		}
		GameObject.Find ("nopeusteksti").GetComponent<TextMesh> ().text = "Nopeutesi on: " + 
			GameObject.Find("OmaAuto").GetComponent<Rigidbody>().velocity.magnitude.ToString("F1") + 
			" km/h";
	}

}
