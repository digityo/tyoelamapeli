# Työelämäpeli #

Työelämäpeli on Digiosaajaksi työelämään -hankkeessa kehitetty oppimispeli tai sarja pelejä. Peli on kehitetty Kaakkois-Suomen ammattikorkeakoulussa (XAMK). Hankkeesta voit lukea lisää osoitteesta https://www.digityo.fi.

Peli on tehty Unity 5.6.1:llä. Tänne Bitbuckettiin tulee ohjelman lähdekoodit ja dokumentaatio. Bitbucketista käytetään myös issue tracking -systeemiä eli tätä kautta voi ilmoittaa ohjelmasta löytyneistä bugeista ja virheistä sekä ehdottaa parannusehdotuksia.

Kokorajoitusten vuoksi lähdekoodeissa on vain Asset ja ProjectSettings -kansiot. Kun Unityllä avaa projektin, Unity raksuttaa jonkun aikaa kun se rakentaa projektia.

Pelin moninpeliversio löytyy osoitteesta https://bitbucket.org/digityo2/tyoelamapeli-moninpeli